// Elementos
function navApp(array){
	var htmlBrand = empty(array.htmlBrand);
	var imgUser = empty(array.imgUser);
	var nameUser = empty(array.nameUser);
	var htmlNavLeft = empty(array.htmlNavLeft);
	var htmlNavRight = empty(array.htmlNavRight);
	var htmlNavRightAll = empty(array.htmlNavRightAll);

	return `
		<div class="navApp">
			<div class="nav-inner">
				<div class="nav-brand">`+ htmlBrand +`</div>
				<div class="nav-left">`+ htmlNavLeft +`</div>
				<div class="nav-right">
					`+ htmlNavRight +`
					<div class="perfil">
						<div class="img-user" style="background:url(`+ imgUser +`) no-repeat center center"></div>
						<div class="name">`+ nameUser +`</div>
					</div>
					`+ htmlNavRightAll +`
				</div>
			</div>
		</div>
	`;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function Title(array){
	var title = empty(array.titulo);
	var descripcion = empty(array.descripcion);
	var classTitle = empty(array.classTitle);
	var classInner = empty(array.classInner);
	var extras = empty(array.extras);
	var add ='';
	  	add +=`
			<div class="Title `+classTitle+`">
				<div class="inner `+classInner+`">
					<h1>`+title+`</h1>`;
	add += extras;
	if (descripcion!=""){
	  	add +=`
				<div class="descripcion">`+ descripcion +`</div>
	  	`;
	}
	add +=`		
			</div>
		</div>
	`;

	return add;
}



function btnAyuda(array){
	return `
		<div class="boxBtnAyuda"><a class="btn btnAyuda btn-info btn-info btn-circle" id="btnAyuda"><span>?</span></a></div>
	`;
}


// Funciones
function empty(vars){
	if (vars==undefined || vars==""){ 
		return "";
	}else{
		return vars;
	}
}

function BtnSigAnt(array) {
 
  var idioma = array.idioma;
  var linkSig = empty(array.linkSig);
  var estadoSig = empty(array.estadoSig);
  var linkAnt = empty(array.linkAnt);
  var saltarLink = empty(array.saltarLink);
  var estadoAnt = empty(array.estadoAnt);
  var iconSig = empty(array.iconSig);
  var iconAnt = empty(array.iconAnt);
  var html = "";
  if (linkSig != "") {
    html += '<a class="btn btn-primary btnSig ' + estadoSig + '" lang="' + idioma + '"><span>Siguiente</span>'+iconSig+'</a>';
  } else { html += ""; }

  if (saltarLink != "") {
    html += '<a class="btn btn-link btnSaltar" lang="' + idioma + '" href="'+saltarLink+'"><span>Saltar</span></a>';
  }  

  if (linkAnt != "") {
    html += '<a class="btn btn-primary btnAnt ' + estadoAnt + '" lang="' + idioma + '"><span>Anterior</span>'+iconAnt+'</a>';
  } else { html += ""; }

  return `
    <div class="btnSigAnt">
      <div class="container">
        `+html+`
      </div>
    </div>    
  `;
}

function errorInput(array){
	var tipoError = empty(array.tipoError);
	var inputId = empty(array.inputId);
	var num = empty(array.num,1);
	var texto="";

	if (tipoError=="error-sinvalor"){
		texto = "Falta contenido en un campo, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}	
	if (tipoError=="error-email-sinvalor"){
		texto = "Falta contenido en e-mail, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}	
	if (tipoError=="error-novalido"){
		texto = "Nombre no valido, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}	
	if (tipoError=="error-email-invalido"){
		texto = "E-mail no valido, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}	
	if (tipoError=="error-seleccionar-rol"){
		texto = "Falta Seleccionar Rol, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}	
	if (tipoError=="error-mail"){
		texto = "Correo ya existe, intentelo una vez más por favor.";
		document.getElementById(inputId).className += " error";
	}
	alert_page({
      text: texto,
      icon: 'icn icn-danger',
      stop: 0,
      num: num,
      animation_in: 'bounceInRight',
      animation_out: 'bounceOutRight',
      tipe: 'danger',
      time: '3700',
      position: 'top-left'
  	});
  return ``;
}

// validaciones

function validarNombre(campo){
	if (campo ==""){
		return "error-sinvalor";
	}
	// var patron =/^[a-z0-9ü]{3}\s[a-z0-9ü]{3}$/;
	var patron =/^[A-Za-zéáíóúüñ]+\s[A-Za-zéáíóúüñ]+$/;
 	if(campo.match(patron)){
		//alert("Usuario bien escritos");
		return "valido-nombre";
	}else{
	// 	//alert("Usuario mal puestos");
		return "error-novalido";
	}
}

function validarURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}

function validarEmail(str){
	if (str ==""){
		return "error-email-sinvalor";
	}
	var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (str.match(emailRegex)) {
      return "valido-email";
    } else {
      return "error-email-invalido";
    }
}

function validarSeleccion(valor){
	if (valor=="0"){
		return "error-noseleccion";
	}else{
		return "valido-seleccion";
	}
}