function alert_page(array){
	//console.info(array);
	var tipe = array.tipe;
	var text = array.text;
  var icon = array.icon; 
  var num = empty(array.num,1);  
  var stop = array.stop;
  var animation_in = array.animation_in; 
  var animation_out =  array.animation_out;
  var time =  array.time;
  var position = array.position;
  var contAlert =0;
  $(".boxAlert").each(function(){ contAlert++; });
  if (contAlert==0){
    $("body").append('<div class="boxAlert '+position+'"></div>');
  }
  $("body .boxAlert").append("<div num='"+num+"' class='m-alert m-alert-num-"+num+" m-alert-"+tipe+" animated "+animation_in+" '><i class='icon "+icon+"'></i><span class='text'>"+text+"</span></div>");
  $(".alert").removeClass(animation_in);

  if (stop!=1){
    setTimeout(function() {
    	// console.log(time);
      $(".m-alert").addClass(animation_out);
      setTimeout(function() {
      	$(".boxAlert").html("");
        $(".boxAlert").remove();
      },450);
    },time);
  }
}
