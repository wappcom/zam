//funciones

function return_view(url) {
    var result = null;
    var scriptUrl = url;

    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    // var parametros = { type: 'get', dataType:'html', async: 'false', cache: 'default' };

    // fetch(scriptUrl, parametros)
    //     .then(function(response) {
    //         if (response.ok) { return response.text(); } else { throw "Error en la llamada Ajax"; }
    //     })
    //     .then(function(msg) {
    //         console.log(msg);
    //         var msg = $.trim(msg);
    //         result = msg;
    //     })
    //     .catch(function(err) {
    //         result = err;
    //     });

    return result;
}