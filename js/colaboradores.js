$(document).ready(function() {

    var url_tab = sitio + "views/agregar-tab.htm";
    var cont_tab = 1;
    var color_1 = "#536DAC";

    $('.loading').hide();

    // $.get(url_tab, function(respuesta) {
    //   var s = respuesta.replace(/\#id#/g, cont_tab);
    //   var s = s.replace(/\#color#/g, color_1);
    //   console.log(s);
    //   // $(".tabs").append(str);
    // });

    var tab_1 = return_view(url_tab);
    var str = tab_1.replace(/\#id#/g, cont_tab);
    var str = str.replace(/\#color#/g, color_1);
    $(".tabs").append(str);
    // $(".btn-eliminar-equipo[item=1]").remove();
    //console.log(s);

    //html
    // console.log(user.userName);
    var add = '';
    add += navApp({
        htmlBrand: '<img class="logo" src="' + sitio + '/images/logo-o.svg" >',
        nameUser: user.userName,
        imgUser: user.userImg,
    });

    add += '<div class="pub-inner animated fadeIn">';
    add += Title({
        titulo: 'Colaboradores',
        descripcion: 'Crea tus equipos de trabajo',
        classInner: 'container',
        extras: btnAyuda()
    });
    add += tabsColaboradores();
    add += boxContainer();
    add += '</div>';
    add += BtnSigAnt({
        linkSig: '#',
        linkAnt: '',
        idioma: idioma,
        estadoSig: '',
        estadoAnt: '',
        saltarLink: sitio + 'plantillas/pag=' + user_id,
        iconSig: '<i class="icn icn-chevron-right"></i>',
        iconAnt: '<i class="icn icn-chevron-left"></i>',
    });

    $(".pub-colaboradores").append(add);

    //console.log(".btnSig");
    // $(".btnSig").addClass('disabled');

    $(".content-tab-1").addClass('active');

    $('.btn-agregar-equipo').click(function(event) {
        cont_tab++;
        var str = tab_1.replace(/\#id#/g, cont_tab);
        var str = str.replace(/\#color#/g, color_1);
        // console.log(item);
        $(".tabs").append(str);
        $(".boxContainer .container").append(contenedorTab(cont_tab, 1));
    });

    $('body').on('click', '.btn-agregar-colaboradores', function() {
        var item = $(this).attr('item');
        var equipo = $(this).attr('equipo');
        var it = parseInt(item) + 1;
        // console.log(equipo+":"+item);
        $(this).attr('item', it);
        $(".boxItems[equipo=" + equipo + "]").append(itemColaborador(equipo, it));
        $(".boxItems[equipo=" + equipo + "] .item-" + it).append(btnEliminarColaborador(equipo, it));
    });

    $('body').on('click', '.btn-eliminar-colaborador', function() {
        var item = $(this).attr('item');
        var equipo = $(this).attr('equipo');
        //console.log(item+":"+equipo); 
        $('#coladorador-' + equipo + "-" + item).remove();
    });

    $('body').on('click', '.btnSig', function() {
        //console.log("btnSig");
        var contadorInputs = 0;
        var cantEquipos = 0;
        var cantItems = 0;
        var retornoNombre = new Array();
        var retornoEmail = new Array();
        var retornoSelect = new Array();
        var valores = new Array();

        var equiposArray = new Array();

        $(".tab-item").removeClass('error-tab');

        //var nombresArray =new Array();
        //var emailsArray =new Array();
        //var rolesArray =new Array();

        var numEq = 0;
        var itemValidos = 0;

        $(".boxItems").each(function(index, el) {
            cantEquipos++;
            var idEquipo = $(this).attr("equipo");
            equiposArray[cantEquipos] = idEquipo;

            let contItem = 0;
            $(".content-tab-" + idEquipo + " .item").each(function(index, el) {
                cantItems++;
                contItem++;
            });

            equiposArray[cantEquipos] = idEquipo + "," + contItem;

        });



        //console.log("cantEquipos:"+cantEquipos+","+"cantItems:"+cantItems);
        //console.log(equiposArray);
        let contItem = 0;
        $(".form-nombre").each(function() {
            contadorInputs++;

            let inputNombre = $(this).val();
            let inputId = $(this).attr("id");
            let item = $(this).attr("item");
            let equipo = $(this).attr("equipo");

            // console.log(inputId+","+inputNombre+","+equipo);
            //console.log(item+","+equipo);
            //console.log("itemNombre:"+item);
            retornoNombre = validarNombre(inputNombre).split("-");
            //console.log(retornoNombre);
            if (retornoNombre[0] == "error") {
                errorInput({
                    tipoError: 'error-' + retornoNombre[1],
                    inputId: inputId
                });
                $(this).attr("estado", "error");
                flagErrorEquipo(equipo);
            }

            if (retornoNombre[0] == "valido") {
                $(this).attr("estado", "valido");
                //nombresArray[contItem] = equipo+","+item+","+inputNombre;
                contItem++;
            }

        });

        contItem = 0;
        $(".form-email").each(function() {
            let inputEmail = $(this).val();
            let inputId = $(this).attr("id");
            let item = $(this).attr("item");
            let equipo = $(this).attr("equipo");
            retornoEmail = validarEmail(inputEmail).split("-");
            //console.log(retornoEmail);
            //console.log("itemEmail:"+item);
            if (retornoEmail[0] == "error") {
                errorInput({
                    tipoError: 'error-' + retornoEmail[1] + '-' + retornoEmail[2],
                    num: contadorInputs,
                    inputId: inputId
                });
                $(this).attr("estado", "error");
                //emailsArray[equipo,item] = "";
            }
            if (retornoEmail[0] == "valido") {
                $(this).attr("estado", "valido");
                // emailsArray[contItem] = equipo+","+item+","+inputEmail;
                contItem++;
            }
        });

        contItem = 0;
        $(".form-select").each(function() {
            let inputSelect = $(this).val();
            let inputId = $(this).attr("id");
            let item = $(this).attr("item");
            let equipo = $(this).attr("equipo");
            // console.log(inputId+""+inputSelect);
            // console.log("itemSelect:"+item);
            retornoSelect = validarSeleccion(inputSelect).split("-");
            //console.log(retornoSelect);
            if (retornoSelect[0] == "error") {
                errorInput({
                    tipoError: 'error-seleccionar-rol',
                    num: contadorInputs,
                    inputId: inputId
                });
                $(this).attr("estado", "error");
                //rolesArray[equipo,item] = "";
            }
            if (retornoSelect[0] == "valido") {
                $(this).attr("estado", "valido");
                //rolesArray[contItem] = equipo+","+item+","+inputSelect;
                contItem++;
            }
        });

        //numEq = equiposArray.length; 



        //console.log("Numero de Equipos: "+ numEq );
        $("input[estado=valido]").each(function() {
            numEq++;
        });

        $("select[estado=valido]").each(function() {
            numEq++;
        });

        itemValidos = cantItems * 3;

        console.log("iVal:" + itemValidos + " numVal:" + numEq);

        if (numEq == itemValidos) {
            // fnGuardarEquipos(nombresArray,emailsArray,rolesArray);
            fnGuardarEquipos();
        }

        // window.location.href = sitio + 'plantillas';

    });

    $('body').on('keydown', '.form-nombre', function(e) {
        var inputId = $(this).attr("id");
        $("#" + inputId).removeClass('error');
    });

    $('body').on('keydown', '.form-email', function(e) {
        var inputId = $(this).attr("id");
        $("#" + inputId).removeClass('error');
    });

    $('body').on('change', '.form-select', function(e) {
        var inputId = $(this).attr("id");
        $("#" + inputId).removeClass('error');
    });

    $('body').on('keydown', '.input-equipo', function(e) {
        var item = $(this).attr('item');
        var valor = capitalizeFirstLetter($(this).val());
        if (e.which == 13) {
            $(this).attr('type', 'hidden');
            doneEquipo(item, valor);
        }
    });

    $('body').on('click', '.btn-eliminar-equipo', function(event) {
        var item = $(this).attr('item');
        //console.log(item);
        $('.tab[item=' + item + ']').remove();
        $('.content-tab-' + item).remove();
        cont_tab--;
        $('.tab[item="1"]').addClass('active');
        $('.contentTab').removeClass('active');
        $('.content-tab-1').addClass('active');
    });

    $('body').on('click', '.tab-item', function(event) {
        var item = $(this).attr('item');
        // console.log(item);
        $('.tab').removeClass('active');
        $('.tab[item=' + item + ']').addClass('active');
        $('.contentTab').removeClass('active');
        $('.content-tab-' + item).addClass('active');
    });

    $('body').on('blur', '.input-equipo', function(event) {
        event.preventDefault();
        var item = $(this).attr('item');
        var valor = $(".input-equipo[item=" + item + "]").val();

        $(".input-equipo[item=" + item + "]").attr('type', 'hidden');
        doneEquipo(item, valor);
        /* Act on the event */
    });

    $('body').on('click', '.btn-color', function(event) {
        var item = $(this).attr('item');
        $('.box-tab-color[item=' + item + ']').html('<a class="btn-color-select" item="' + item + '" style="background-color:#536DAC" color="#536DAC"></a><a class="btn-color-select" item="' + item + '" style="background-color:#00BCD4" color="#00BCD4"></a><a class="btn-color-select" item="' + item + '" style="background-color:#8BC34A" color="#8BC34A"></a><a class="btn-color-select" item="' + item + '" style="background-color:#FFC107" color="#FFC107"></a><a class="btn-color-select" item="' + item + '" style="background-color:#F44336" color="#F44336"></a>');
        $('.box-tab-color[item=' + item + ']').show();

        $('.btn-color-select').click(function(event) {
            var item = $(this).attr('item');
            var color = $(this).attr('color');
            $('.inputColor[item=' + item + ']').val(color);
            $('.tab[item=' + item + ']').attr('style', 'border-color:' + color);
            $('.btn-color[item=' + item + ']').attr('style', 'background-color:' + color);
            $('.box-tab-color').hide();
            $('.btn-color[item=' + item + ']').show();
        });

    });

    $('body').on('click', '.btn-editar-equipo', function(event) {
        var item = $(this).attr('item');
        $('.nombre-equipo[item=' + item + ']').hide();
        $('.tab[item=' + item + '] .input-equipo').attr('type', 'text');
        $('.tab[item=' + item + '] .btn-color').show();
        $(this).hide();
    });



});


// elementos

function tabsColaboradores(array) {
    return `
  <div class="tabs-group">
    <div class="container">
      <div class="tabs">
        <div class="tab active" item="1" style="border-color:#536DAC">
            <span class="tab-item tab-name" item="1">Equipo</span>
            <span class="nombre-equipo tab-item" item='1'>1</span>
            <div class="box-tab-color" item='1' ></div>
            <a class="btn-color" item='1' style="background-color:#536DAC"></a>
            <input type="hidden" item="1" class="inputColor" value="#536DAC"> 
            <input type="hidden" item="1" class="input-equipo" value="1"> 
            <a class="editar btn-editar-equipo" item="1"><i class="icn icn-pencil"></i></a>
        </div>
      </div>
      <a class="btn-agregar-equipo" item='1'><i class='btn-info-o btn-rounded icn icn-plus'></i> <span>Nuevo Equipo</span></a>
    </div>
  </div>
  `;
}


function boxContainer(array) {

    return `
    <div class="boxContainer">
      <div class="container">
        ` + contenedorTab(1, 1) + `
      </div>
    </div>
  `;
}

function btnEliminarColaborador(equipo, item) {

    return `
    <a class='eliminar btn-eliminar-colaborador' item='` + item + `' equipo='` + equipo + `'><i class='icn icn-close-circle'></i></a>
  `;
}

function contenedorTab(equipo, item) {

    return `
    <div class="contentTab content-tab-` + equipo + `" id="content-tab-` + equipo + `" equipo="` + equipo + `" >
      <div class="box-agregar-colb">
        <a class='btn-agregar-colaboradores' equipo="` + equipo + `" item='` + item + `'>
          <i class="btn btn-rounded  btn-info-o  icn icn-plus"></i>
          <span>Agregar Colaborador</span>
        </a>
      </div>
      <div class="boxItems" equipo="` + equipo + `" item='` + item + `'>
        ` + itemColaborador(equipo, item) + `
      </div>
    </div>
  `;
}

function itemColaborador(equipo, item) {
    var selectRolActual = selectRol.replace(/\inputRol-1/g, 'inputRol-' + equipo + '-' + item);
    var selectRolActual = selectRolActual.replace(/\" item/g, '" item=' + item);
    var selectRolActual = selectRolActual.replace(/\equipo/g, 'equipo=' + equipo);

    return `
    <div class="item-` + item + ` item itemCompleto" item="` + item + `" equipo="` + equipo + `"  id="coladorador-` + equipo + `-` + item + `">
      <input type="text" class='form-nombre form-control itemEquipo' tipo="nombre" item="` + item + `" equipo="` + equipo + `" id='inputNombre-` + equipo + `-` + item + `' placeholder="Nombre completo del colaborador">
      <input type="text" class='form-control form-email itemEquipo' tipo="email" item="` + item + `" equipo="` + equipo + `" id='inputEmail-` + equipo + `-` + item + `' placeholder="E-mail del colaborador">
      ` + selectRolActual + `      
    </div>
  `;
}

function doneEquipo(item, valor) {
    $('.nombre-equipo[item=' + item + ']').html(valor);
    $('.nombre-equipo[item=' + item + ']').show();
    $('.btn-editar-equipo[item=' + item + ']').show();
    $('.btn-color').hide();
    alert_page({
        text: 'Cambio de nombre de equipo, relizado exitosamente.',
        icon: 'icn icn-checkmark-circle',
        animation_in: 'bounceInRight',
        animation_out: 'bounceOutRight',
        tipe: 'success',
        time: '3500',
        position: 'top-left'
    });
}

function registrarEquipos() {
    var equipo = new Array();
    var contEq = 0;
    // $(".inputColor").each(function(){
    //   var item = $(this).attr("item");
    //   var valor = $(this).val();
    //   equipo[item]["color"] = valor;
    // });    

    $(".input-equipo").each(function() {
        var item = $(this).attr("item");
        var valor = $(this).val();
        var color = $(".inputColor[item='" + item + "']").val();
        equipo.push(valor + "," + color);
    });

    return equipo;
}

function registrarItems() {
    var itemArray = new Array();

    $(".itemCompleto").each(function(index, el) {
        let equipo = $(this).attr("equipo");
        let tipo = $(this).attr("tipo");
        let item = $(this).attr("item");

        let nombre = $(".itemEquipo[tipo=nombre][item=" + item + "][equipo=" + equipo + "]").val();
        let email = $(".itemEquipo[tipo=email][item=" + item + "][equipo=" + equipo + "]").val();
        let rol = $(".itemEquipo[tipo=select][item=" + item + "][equipo=" + equipo + "]").val();

        cadena = equipo + "," + item + "," + nombre + "," + email + "," + rol;
        itemArray.push(cadena);
    });

    //console.log(itemArray);
    return itemArray;
}

// fnGuardarEquipos = function(cadenaNombre,cadenaEmail,cadenaRol){
fnGuardarEquipos = function() {
    //console.log(cadena);
    //let countCad = cadena.length;
    //var itemArray =new Array();

    //registrarEquipos();
    //registrarItems();

    console.log("user:" + user_id);



    const data = new FormData();
    data.append('inputUser', user_id);
    data.append('inputEquipo', JSON.stringify(registrarEquipos()));
    data.append('inputItem', JSON.stringify(registrarItems()));
    //data.append('inputNombre', JSON.stringify(cadenaNombre));
    // data.append('inputEmail', JSON.stringify(cadenaEmail));
    // data.append('inputRol', JSON.stringify(cadenaRol));

    //const data = new URLSearchParams("nombre=miguel angel&nacionalidad=español");
    const myHeaders = new Headers();
    //myHeaders.append("Content-Type", "text/plain");
    const ruta = sitio + 'controler/ajax/ajax-registrar-colaboradores.php';
    var parametros = { method: 'POST', headers: myHeaders, mode: 'cors', cache: 'default', body: data };

    fetch(ruta, parametros)
        .then(function(response) {
            if (response.ok) { return response.text(); } else { throw "Error en la llamada Ajax"; }
        })
        .then(function(msg) {
            console.log(msg);
            var msg = $.trim(msg);
            var dt = msg.split(",");
            if (dt[0] == "error") {
              if(dt[1]=="mail"){
                //$("#"+dt[2]).addClass('error');
                errorInput({
                    tipoError: 'error-' + dt[1],
                    num: 1,
                    inputId: dt[2]
                });
              }
            }
        })
        .catch(function(err) {
            console.log(err);
        });
}


/* Acciones */

flagErrorEquipo = function(equipo) {
    if (equipo != 1) {
        $(".tab[item=" + equipo + "] .tab-name").addClass('error-tab');
    }
}