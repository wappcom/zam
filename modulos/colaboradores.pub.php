<?php 

header("Content-Type: text/html;charset=utf-8");
require_once(_RUTA_NUCLEO."clases/class-constructor.php");
$fmt = new CONSTRUCTOR;

require_once(_RUTA_NUCLEO."modulos/adm/cuenta-empresas.class.php");
$cuenta = new CUENTA_EMPRESA($fmt);

// $id_usu = $fmt->sesion->get_variable("usu_id");
$id_usu = $_GET["user"];
$id_rol = $fmt->usuario->id_rol_usuario($id_usu);

$usu_nombre    = $fmt->usuario->nombre_usuario($id_usu);
$usu_apellidos = $fmt->usuario->apellidos_usuario($id_usu);
$usu_imagen    = $fmt->usuario->imagen_usuario_mini($id_usu);
$rol_nombre    = $fmt->usuario->nombre_rol($id_rol);

define("_USU_NOMBRE_COMPLETO",$usu_nombre." ". $usu_apellidos);
define("_USU_IMAGEN",$usu_imagen);
define("_USU_ID",$id_usu);
define("_USU_ID_ROL",$id_rol);

$cnt_id = $cuenta->id_cuenta_usuario(_USU_ID);
$cntEmp = $cuenta->cuenta_empresa_id($cnt_id);
//var_dump($cntEmp);

// echo $cntEmp['mod_cnt_emp_id'];
$empresa = $cuenta->empresa($cntEmp['mod_cnt_emp_id']);
// var_dump($empresa);
$cnt_nombre = $empresa["emp_nombre"];

$sql="DELETE FROM mod_equipo";
$fmt->query->consulta($sql,__METHOD__);

$up_sqr6 = "ALTER TABLE mod_equipo AUTO_INCREMENT=1";
$fmt->query->consulta($up_sqr6,__METHOD__);
 
?>
<link rel="stylesheet" href="<?php echo _RUTA_WEB; ?>css/navApp.css?reload=<?php echo $vrand;?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo _RUTA_WEB; ?>css/Colaboradores.css?reload=<?php echo $vrand;?>" type="text/css" media="screen" />
<script type="text/javascript">
	var user_id = <?php echo _USU_ID; ?>;
	var user = {userName:'<?php echo $usu_nombre; ?>',userImg:'<?php echo $usu_imagen; ?>'};
	var selectRol = `
		<?php  
			$select = $fmt->form->select_nodo(array('label' => '',
													'id' => 'inputRol-1',
													'atributos' => 'item equipo',
													'from'=>'rol',
													'prefijo'=>'rol_',
													'classSelect'=>'itemEquipo',
													'nivel_hijos' => '1',
													'label_item_inicial'=>'Seleccionar Rol',
													'id_inicio'=>'3'));
			echo $select;
		?>
	 `
</script>
<div class="loading on"><span>Espere por favor.</span></div>
<div class="pub pub-colaboradores"></div>
<script src="<?php echo _RUTA_WEB; ?>js/index.js" ></script>  
<script src="<?php echo _RUTA_WEB; ?>js/colaboradores.js" ></script>  

