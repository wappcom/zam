<?php
header("Content-Type: text/html;charset=utf-8");

class EQUIPO{

	var $fmt;
	var $id_mod;
	var $id_item;
	var $id_estado;
	var $cuentaEmpresa;
	var $empresa;
	var $rutaBase = _RUTA_HOST."modulos/empresas/";

	function EQUIPO($fmt,$id_mod=0,$id_item=0,$id_estado=0){
		$this->fmt = $fmt;
		$this->id_mod = $id_mod;
		$this->id_item = $id_item;
		$this->id_estado = $id_estado;

		require_once($this->rutaBase."controler/class/cuenta-empresas.class.php");
		$this->cuentaEmpresa = new CUENTA_EMPRESA($this->fmt);

		require_once($this->rutaBase."controler/class/empresas.class.php");
		$this->empresa = new EMPRESA($this->fmt);
	}

	function busqueda(){

		echo $this->fmt->page->headerPageMod(  array('id_mod' => $this->id_mod,
												'modo' => "estandar-mod",
												'css' => _RUTA_WEB."modulos/empresas/src/css/equipos.css" ));  

		$this->fmt->form->head_table("table_id");
    	$this->fmt->form->thead_table('Id:Nombre:Cuenta:Activar:Acciones');
    	$this->fmt->form->tbody_table_open();

    	$consulta = "SELECT * FROM mod_equipo";
	  	$rs =$this->fmt->query->consulta($consulta);
	  	$num=$this->fmt->query->num_registros($rs);
	  	$tabla="";
	  	if($num>0){
	  		for($i=0;$i<$num;$i++){
	  			$row=$this->fmt->query->obt_fila($rs);
	  			$row_id 		= $row["mod_equ_id"];
	  			$row_nombre 	= $row["mod_equ_nombre"];
	  			$row_cuenta 	= $row["mod_equ_cnt_id"];
	  			$row_color	 	= $row["mod_equ_color"];
	  			$row_estado	 	= $row["mod_equ_estado"];
	  			$activar	 	= $row["mod_equ_activar"];

	  			$cuentaArray = $this->cuentaEmpresa->datosId($row_cuenta);
	  			$empresaArray = $this->empresa->datosId($cuentaArray["mod_cnt_emp_id"]);
	  			$empNombre = $empresaArray["emp_nombre"];

	  			$estado = $this->fmt->modulo->estadoPublicacion( array( 'estado' => $row_estado, 
	  																	'id_mod'=> $this->id_mod,
	  																	'class'=> '',
	  																	'id'=> $row_id,
	  																	'activar' => $activar ));
				$tabla .= '<tr class="row row-'.$row_id.'">';
				$tabla .= '  <td class="col-id">'.$row_id.'</td>';
				$tabla .= '  <td class="col-nombre"><span class="colorEquipo" style="background-color:'.$row_color.'"></span> Equipo '.$row_nombre.'</td>';
				$tabla .= '  <td class="col-cuenta">'.$empNombre.'</td>';
				$tabla .= '  <td class="col-activar">'.$estado.'</td>';
				$tabla .= '  <td class="col-acciones"></td>';
				$tabla .= '</td>';
	  		}
	  	}

	  	echo $tabla;

    	$this->fmt->form->tbody_table_close();
    	$this->fmt->form->footer_table();
		echo $this->fmt->modulo->scriptPage(array("id_mod" => $this->id_mod));
		$this->fmt->class_modulo->script_table("table_id",$this->id_mod,"desc","3","25",true);
		$this->fmt->class_modulo->script_accion_modulo();
		$this->fmt->page->footerPageMod();
	}

	function crearEquipo($vars){
		$nombre = $vars["nombre"];
	    $descripcion = $vars["descripcion"];
	    $estado = $vars["estado"];
	    $color = $vars["color"];
	    $activar = $vars["activar"];
	    $cnt_id = $vars["cnt_id"];
	    //$usu_id = $vars["usu_id"];

	    $ingresar = "mod_equ_nombre, mod_equ_descripcion, mod_equ_estado, mod_equ_color, mod_equ_activar, mod_equ_cnt_id";
	    $valores  = "'".$nombre."','".
	          $descripcion."','".
	          $estado."','".
	          $color."','".
	          $activar."','".
	          $cnt_id."'";
	    $sql="insert into mod_equipo (".$ingresar.") values (".$valores.")";
	    $this->fmt->query->consulta($sql,__METHOD__);

	    $sql="select max(mod_equ_id) as id from mod_equipo";
	    $rs= $this->fmt->query->consulta($sql,__METHOD__);
	    $fila = $this->fmt->query->obt_fila($rs);
	    return $fila ["id"];
	}

	function crearUsuarioEquipo($vars){
		$nombre = $vars["nombre"];
		$email = $vars["email"];
		$rol = $vars["rol"];
		$idEquipo = $vars["idEquipo"];
	}

}