<?php
require_once("../../config.php");
header("Content-Type: text/html;charset=utf-8");


require_once(_RUTA_NUCLEO."clases/class-constructor.php");
$rutaBase = _RUTA_HOST."modulos/empresas/";

require_once($rutaBase."controler/class/cuenta-empresas.class.php");
require_once($rutaBase."controler/class/empresas.class.php");
require_once($rutaBase."controler/class/equipos.class.php");

$fmt = new CONSTRUCTOR;
$empresa = new EMPRESA($fmt);
$equipos = new EQUIPO($fmt);
$cuentaEmpresa = new CUENTA_EMPRESA($fmt);


$equipo = json_decode($_POST["inputEquipo"]);
$item = json_decode($_POST["inputItem"]);

$user =  $_POST["inputUser"];
$cntArray = $cuentaEmpresa->datosCuentaUsuario($user);
$cnt_id = $cntArray["mod_cnt_id"];
$emp_id = $cntArray["mod_cnt_emp_id"];
$empresaArray = $empresa->datos($emp_id);
$nomEmpresa = $empresaArray["emp_nombre"];

$contEquipos = count($equipo);
$contItem = count($item);

for ($i=0; $i < $contItem; $i++) { 
	$itemArray = explode(",",$item[$i]);
	//echo $itemArray[3];
	if($fmt->usuario->exiteUsuarioEmail($itemArray[3])){
		echo "error,mail,inputEmail-".$itemArray[0]."-".$itemArray[1].",".$itemArray[3];
		exit(0);
	}
}


if ($cntArray!=0){
	
	for ($i=0; $i < $contEquipos ; $i++) { 
		//echo $equipo[$i];
		$arrayEquipo = explode(",",$equipo[$i]);
		$nomEq = $arrayEquipo[0];
		$colorEq = $arrayEquipo[1];
		$des = "equipo de empresa ".$nomEmpresa;
		$idEquipo[$i] = $equipos->crearEquipo(array('nombre' => $nomEq,
												'color'=> $colorEq,
												'descripcion'=> $des,
												'estado'=> '1',
												'activar'=> '1',
												'cnt_id'=> $cnt_id ));

		for ($j=0; $j < $contItem; $j++) { 
			$numIt = $i + 1;
			$itemArray = explode(",",$item[$j]);
			//echo $numIt."<br/>";
			//var_dump($itemArray);
			
			if ($itemArray[0]==$numIt){
				$nomUsu = $itemArray[2];
				$emailUsu = $itemArray[3];
				$rolUsu = $itemArray[4];
				$idEq = $idEquipo[$i];

				$equipos->crearUsuarioEquipo(array('nombre' => $nomUsu, 'email'=> $emailUsu,'rol' => $rolUsu, 'idEquipo' => $idEq ));

				//echo $nomUsu."~".$emailUsu."~".$rolUsu."~".$idEq."<br/>";
 			}
		}
	}
	
}




// var_dump($idEquipo);
//var_dump($item);
// var_dump($equipo);
// var_dump($equipo[0]);
// var_dump(explode(",", $equipo[0]));



